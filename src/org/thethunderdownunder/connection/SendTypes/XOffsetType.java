package org.thethunderdownunder.connection.SendTypes;

/**
 * Created by Cameron on 24/11/17.
 */

public class XOffsetType extends SendType {
    public int xOffset;

    public XOffsetType() {
        //no args constructor
    }

    public XOffsetType(int xOffset) {
        this.xOffset = xOffset;
        this.type = this.getClass().getSimpleName();
    }
}
