package org.thethunderdownunder.connection.SendTypes;

/**
 * Created by ctech on 11/22/17.
 */

/**
 * So this doesn't have to exist. It is simply a easy way to force what can and can't be sent
 * We only want JSONs to be sent. Not random variables. Make all send types extend this, then
 * when converting to a JSON we can check if our object does actually extend this, and reject it if
 * it doesn't.
 * TODO: FIND WAY TO IMPLEMENT NO-ARGS DEFAULT CONSTRUCTOR
 */
public abstract class SendType {
    public String type;
}
