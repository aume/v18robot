package org.thethunderdownunder.connection;

import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.Timer;

import org.thethunderdownunder.Robot;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import edu.wpi.first.wpilibj.interfaces.Gyro;
import org.thethunderdownunder.connection.SendTypes.*;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

/**
 * This controls all vision actions, including vision updates, capture, and interfacing with the Android phone with
 * Android Debug Bridge. It also stores all VisionUpdates (from the Android phone) and contains methods to add to/prune
 * the VisionUpdate list. Much like the subsystems, outside methods get the VisionServer instance (there is only one
 * VisionServer) instead of creating new VisionServer instances.
 */

public class VisionServer implements Runnable {

    private static VisionServer s_instance = null;
    private ServerSocket m_server_socket;
    private boolean m_running = true;
    private int m_port;
    //private ArrayList<VisionUpdateReceiver> receivers = new ArrayList<>();
    AdbBridge adb = new AdbBridge();
    double lastMessageReceivedTime = 0;
    private boolean m_use_java_time = false;
    
    private Gson gson;

    private ArrayList<ServerThread> serverThreads = new ArrayList<>();
    private volatile boolean mWantsAppRestart = false;
    
    private final ArrayList<Class<? extends SendType>> K_SEND_TYPES = new ArrayList<Class<? extends SendType>>(
    		Arrays.asList(
    				XOffsetType.class
    		)
    ); //TODO: Reflections to do this automagically

    public static VisionServer getInstance() {
        if (s_instance == null) {
            s_instance = new VisionServer(8254);
        }
        return s_instance;
    }

    private boolean mIsConnect = false;

    public boolean isConnected() {
        return mIsConnect;
    }

    public void requestAppRestart() {
        mWantsAppRestart = true;
    }

    protected class ServerThread implements Runnable {
        private Socket m_socket;

        public ServerThread(Socket socket) {
            m_socket = socket;
        }

        public void send(String message) {
            String toSend = message + "\n";
            if (m_socket != null && m_socket.isConnected()) {
                try {
                    OutputStream os = m_socket.getOutputStream();
                    os.write(toSend.getBytes());
                } catch (IOException e) {
                    System.err.println("VisionServer: Could not send data to socket");
                }
            }
        }

        private void handleReceivedObject(SendType obj) {
            if (obj instanceof XOffsetType) {
            	//DO SOMETHING
            	int offset = ((XOffsetType)(obj)).xOffset;
            	double turn = offset/800.0;
            	System.out.println("xoffset: " + offset + " turn: " + turn);
            	if (RobotState.isAutonomous()) {
            		Robot.fl.set(turn);
            		Robot.bl.set(turn);
            		Robot.fr.set(turn);
            		Robot.br.set(turn);
            		
            	}
            }
        }

        public void handleMessage(String message, double timestamp) {
        	lastMessageReceivedTime = getTimestamp();
        	//System.out.println("got a message!");
            //System.out.println(message);
        	if (Objects.equals(message,"heartbeat")) {
        		return;
        	}
        	
        	String type = new JsonParser().parse(message).getAsJsonObject().get("type").getAsString();
        	
        	//System.out.println("Type: " + type);
        	
        	SendType receivedJson = null;
        	
        	for (Class<? extends SendType> sendType : K_SEND_TYPES) {
        		if (Objects.equals(sendType.getSimpleName(), type)) {
        			receivedJson = gson.fromJson(message, sendType);
        		}
        	}
        	
        	if (receivedJson == null) {
        		System.out.println("Received unknown JSON type, doing nothing");
        	} else {
        		//System.out.println("Recieved JSON of type " + type);
        		handleReceivedObject(receivedJson);
        	}
        }

        public boolean isAlive() {
            return m_socket != null && m_socket.isConnected() && !m_socket.isClosed();
        }

		@Override
		public void run() {
			System.out.println("RUN");
			if (m_socket == null) {
                return;
            }
            try {
                InputStream is = m_socket.getInputStream();
                byte[] buffer = new byte[2048];
                int read;
                System.out.println("m_socket connection: " + m_socket.isConnected());
                while (m_socket.isConnected() && (read = is.read(buffer)) != -1) {
                    double timestamp = getTimestamp();
                    lastMessageReceivedTime = timestamp;
                    String messageRaw = new String(buffer, 0, read);
                    //System.out.println("messageRaw");
                    String[] messages = messageRaw.split("\n");
                    for (String message : messages) {
                        handleMessage(message, timestamp);
                    }
                }
                System.out.println("Socket disconnected");
            } catch (IOException e) {
                System.err.println("Could not talk to socket");
            }
            if (m_socket != null) {
                try {
                    m_socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
			
		}
    }

    /**
     * Instantializes the VisionServer and connects to ADB via the specified port.
     * 
     * @param port
     */
    private VisionServer(int port) {
        try {
            adb = new AdbBridge();
            m_port = port;
            m_server_socket = new ServerSocket(port);
            adb.start();
            adb.restartApp();
            adb.reversePortForward(port, port);
            try {
                m_use_java_time = Boolean.parseBoolean(System.getenv("USE_JAVA_TIME"));
            } catch (NullPointerException e) {
                m_use_java_time = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        new Thread(this).start();
        new Thread(new AppMaintainanceThread()).start();
        gson = new Gson();
    }

    public void restartAdb() {
        adb.restartAdb();
        System.out.println("restartADB");
        adb.reversePortForward(m_port, m_port);
    }

    @Override
    public void run() {
        while (m_running) {
            try {
                Socket p = m_server_socket.accept();
                ServerThread s = new ServerThread(p);
                new Thread(s).start();
                serverThreads.add(s);
            } catch (IOException e) {
                System.err.println("Issue accepting socket connection!");
            } finally {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class AppMaintainanceThread implements Runnable {
        @Override
        public void run() {
            while (true) {
                if (getTimestamp() - lastMessageReceivedTime > 5000) {
                	
                    // camera disconnected
                	System.out.println("Disconnected");
                	//System.out.println("Current Time: " + getTimestamp());
                	//System.out.println("Last message Received at: " + lastMessageReceivedTime);
                    adb.reversePortForward(m_port, m_port);
                    mIsConnect = false;
                } else {
                    mIsConnect = true;
                }
                if (mWantsAppRestart) {
                    adb.restartApp();
                    mWantsAppRestart = false;
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private double getTimestamp() {
        if (m_use_java_time) {
            return System.currentTimeMillis();
        } else {
            return Timer.getFPGATimestamp();
        }
    }	
}
